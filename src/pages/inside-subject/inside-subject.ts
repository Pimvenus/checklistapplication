import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CreateChecklistPage} from '../create-checklist/create-checklist';
import {DailyCheckListPage} from '../daily-check-list/daily-check-list';
import {CheckSumPage} from '../check-sum/check-sum';

/**
 * Generated class for the InsideSubjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inside-subject',
  templateUrl: 'inside-subject.html',
})
export class InsideSubjectPage {
  subjectdata:any
  subjectname:any
  subjectid:any
  group:any
code:any

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InsideSubjectPage');
    this.subjectdata=this.navParams.data

  }
  ionViewWillEnter(){
    this.subjectname=this.subjectdata.subject_name
    this.subjectid=this.subjectdata.subject_id
    this.group=this.subjectdata.group
    this.code=this.subjectdata.code

  }
  gocreatecheck(){
    this.navCtrl.push(CreateChecklistPage,this.subjectdata);
  }
  gotodailylist(){
    this.navCtrl.push(DailyCheckListPage,this.subjectdata);
  }
  gotocheck(){
    this.navCtrl.push(CheckSumPage,this.subjectdata)
  }
}