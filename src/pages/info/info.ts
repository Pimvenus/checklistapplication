import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage} from '../home/home';

/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  studentdata:any
 
  name:any
  id:any
  faculty:any
  major:any
  email:any

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
    this.studentdata=this.navParams.data
    this.name=this.studentdata.student_name
    this.id=this.studentdata.student_id
    this.faculty=this.studentdata.fauctly
    this.major=this.studentdata.major
    this.email=this.studentdata.email
    }
    logout(){
      this.navCtrl.setRoot(HomePage)
    }


}
