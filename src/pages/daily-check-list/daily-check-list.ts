import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service';
import { DailyallshowPage } from '../dailyallshow/dailyallshow';

/**
 * Generated class for the DailyCheckListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daily-check-list',
  templateUrl: 'daily-check-list.html',
})
export class DailyCheckListPage {
sjdata:any
subjectname:any
subjectid:any
group:any
code:any
student:Array<any>
daily=[]
  constructor(public navCtrl: NavController, public navParams: NavParams,private checklistservice:ChecklistServiceProvider) {
  }

  ionViewDidLoad() {
    this.sjdata=this.navParams.data
    console.log('ionViewDidLoad DailyCheckListPage');
    this.checklistservice.getCheckname().subscribe((Response)=>{
      this.subjectid=this.sjdata.subject_id
      this.subjectname=this.sjdata.subject_name
      this.group=this.sjdata.group
      this.code=this.sjdata.code
      this.student=this.sjdata.student
        Response.data.forEach(element =>{
          if(element.subject_id==this.subjectid){
      var data={
      datetime:element.date_time,
      sumstu:this.student.length,
      num_stu:element.student.length,
      loststu:this.student.length-element.student.length,
      stu_daily:element.student,
      stu_subject:this.student
  
      }
      this.daily.push(data)
          
          }
        })
          
  
  })
  
}
gotoshowall(item){
  this.navCtrl.push(DailyallshowPage,item);
}


}
