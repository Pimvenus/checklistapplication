// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import { Http} from '@angular/http';
/*
  Generated class for the ChecklistServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChecklistServiceProvider {

  constructor(public http: Http) {
    console.log('Hello ChecklistServiceProvider Provider');
  }
loginstudent(id,password){
 
  return this.http.get("http://192.168.43.181:8000/student/"+id+"/"+password).map((res)=>res.json());
  
}
loginteacher(id,password){
 
  return this.http.get("http://192.168.43.181:8000/teacher/"+id+"/"+password).map((res)=>res.json());
}
 createUserStudent(m,name,id,fauclty,major,email,password){
   var data={ 
     student_id:id,
     student_name:m+name,
     fauctly:fauclty,
     major:major,
     email:email,
     password:password

   }
return this.http.post("http://192.168.43.181:8000/student/add",data).toPromise();
 }
 createUserTeacher(m,name,faculty,email,password){
  var data={ 
    teacher_name:m+name,
     faulty:faculty,
     email:email,
     password:password

   }
   return this.http.post("http://192.168.43.181:8000/teacher/add",data).toPromise();
 }
 createClassSubject(id,namesubject,group,teacher_id,teacher_name,code){
   var data={
     subject_id:id,
     subject_name:namesubject,
     group:group,
     teacher_id:teacher_id,
     teacher_name:teacher_name,
     student:[],
     code:code
   }
   return this.http.post("http://192.168.43.181:8000/subject/add",data).toPromise(); //  return this.http.post("http://localhost:8000/subject/add",data).toPromise();
  }
  getSubject(){
    return this.http.get("http://192.168.43.181:8000/subject").map((res)=>res.json());
  }

  getCodeJoine(){
    return this.http.get("http://192.168.43.181:8000/codejoine").map((res)=>res.json());
  }
  getCodeCheck(){
    return this.http.get("http://192.168.43.181:8000/codecheck").map((res)=>res.json());
  }

  addCodeJoine(code){
    var data={ 
      random_code:code,
     }
     return this.http.post("http://192.168.43.181:8000/codejoine/add",data).toPromise();
   }
   addCodeCheck(code){
    var data={ 
      code:code,
     }
     return this.http.post("http://192.168.43.181:8000/codecheck/add",data).toPromise();
   }
   deleteCodeCheck(code){
 
    return this.http.delete("http://192.168.43.181:8000/codecheck/delete/"+code).toPromise();
  }

   addCheckname(subject_id,subject_name,group,teacher_id,teacher_name,codeCheck){
    var dt = new Date().toLocaleString('en-GB', { timeZone: 'Asia/Bangkok' })
 
    var data={ 
      student:[],
      subject_id:subject_id,
      subject_name:subject_name,
      group:group,
      teacher_id:teacher_id,
      teacher_name:teacher_name,
      date_time:dt,
      code:codeCheck,
     }
     return this.http.post("http://192.168.43.181:8000/checkname/add",data).toPromise();
   }
   clearCode(code){
     console.log(code);
    return this.http.put("http://192.168.43.181:8000/clearCode/"+code,'').toPromise();
   }
   joineclass(student_id,student_name,code){
     var data={
student_id:student_id,
student_name:student_name
     }
     return this.http.put("http://192.168.43.181:8000/joinclasswithsubject/"+code,data).toPromise();
   }
   checklistbyStu(student_id,student_name,code){
    var data={
      student_id:student_id,
      student_name:student_name
           }
  
   return this.http.put("http://192.168.43.181:8000/checknameofStu/"+code,data).toPromise();
  }
getCheckname(){
  return this.http.get("http://192.168.43.181:8000/checkname").map((res)=>res.json());
}
  }
