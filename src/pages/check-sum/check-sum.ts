import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service';
/**
 * Generated class for the CheckSumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-check-sum',
  templateUrl: 'check-sum.html',
})
export class CheckSumPage {
  sjdata:any
  subjectname:any
  subjectid:any
  group:any
  code:any
  student:Array<any>
  daily:Array<any>
  stu_daily=[]
  come=[]
  all_class=[]
  sum_data=[]
  sum_studeninclass:any
  constructor(public navCtrl: NavController, public navParams: NavParams,private checklistservice:ChecklistServiceProvider) {
  }

 
  ionViewDidLoad() {
    this.sjdata=this.navParams.data
    console.log('ionViewDidLoad DailyCheckListPage');
    this.checklistservice.getCheckname().subscribe((Response)=>{
      this.subjectid=this.sjdata.subject_id
      this.subjectname=this.sjdata.subject_name
      this.group=this.sjdata.group
      this.code=this.sjdata.code
      this.student=this.sjdata.student
      this.daily=Response.data
      Response.data.forEach(element =>{
          if(element.subject_id==this.subjectid){
             this.stu_daily.push(element.student)
          }
      })
      this.stunameSplit(this.stu_daily,this.student)
  
  })
  
}
set_sum(){
  for(let i=0;i<this.all_class.length;i++){
    var sum = 0
    this.daily.forEach(element => {

      element.student.forEach(element1 => {

        if(this.all_class[i].student_id == element1.student_id){
          sum++
        }
        
      });

      
    });

    var data1 ={
      id:this.all_class[i].student_id,
      name:this.all_class[i].student_name,
      sum:sum
    }
    this.sum_data.push(data1)
    // this.sum_data.push(this.all_class[i].student_id+" "+this.all_class[i].student_name+" รวม "+sum+" ครั้ง")
  }
}

stunameSplit(arr1,arr2){
  arr1.forEach(element => {
    this.come.push(element)
  });
  
  arr2.forEach(element => {
    this.all_class.push(element)
  });
  this.sum_studeninclass=this.stu_daily.length
  this.set_sum()
}

}
