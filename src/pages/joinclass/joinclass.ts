import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service';
// import {StudentHomePage} from '../student-home/student-home';
/**
 * Generated class for the JoinclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-joinclass',
  templateUrl: 'joinclass.html',
})
export class JoinclassPage {
  studentdata:any
  stu_id:any
  stu_name:any
  constructor(public navCtrl: NavController, public navParams: NavParams,private checklistservice:ChecklistServiceProvider) {
  }

  ionViewDidLoad() {
    this.studentdata=this.navParams.data
    this.stu_id=this.studentdata.student_id
    this.stu_name=this.studentdata.student_name
    console.log('ionViewDidLoad JoinclassPage');
  }
async gotoclassSubject(code_joine){
   await this.checklistservice.joineclass(this.stu_id,this.stu_name,code_joine)
    this.navCtrl.pop( this.studentdata);
    
  }
}
