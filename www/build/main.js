webpackJsonp([19],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckSumPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the CheckSumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CheckSumPage = /** @class */ (function () {
    function CheckSumPage(navCtrl, navParams, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistservice = checklistservice;
        this.stu_daily = [];
        this.come = [];
        this.all_class = [];
        this.sum_data = [];
    }
    CheckSumPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.sjdata = this.navParams.data;
        console.log('ionViewDidLoad DailyCheckListPage');
        this.checklistservice.getCheckname().subscribe(function (Response) {
            _this.subjectid = _this.sjdata.subject_id;
            _this.subjectname = _this.sjdata.subject_name;
            _this.group = _this.sjdata.group;
            _this.code = _this.sjdata.code;
            _this.student = _this.sjdata.student;
            _this.daily = Response.data;
            Response.data.forEach(function (element) {
                if (element.subject_id == _this.subjectid) {
                    _this.stu_daily.push(element.student);
                }
            });
            _this.stunameSplit(_this.stu_daily, _this.student);
        });
    };
    CheckSumPage.prototype.set_sum = function () {
        var _this = this;
        var _loop_1 = function (i) {
            sum = 0;
            this_1.daily.forEach(function (element) {
                element.student.forEach(function (element1) {
                    if (_this.all_class[i].student_id == element1.student_id) {
                        sum++;
                    }
                });
            });
            data1 = {
                id: this_1.all_class[i].student_id,
                name: this_1.all_class[i].student_name,
                sum: sum
            };
            this_1.sum_data.push(data1);
            // this.sum_data.push(this.all_class[i].student_id+" "+this.all_class[i].student_name+" รวม "+sum+" ครั้ง")
        };
        var this_1 = this, sum, data1;
        for (var i = 0; i < this.all_class.length; i++) {
            _loop_1(i);
        }
    };
    CheckSumPage.prototype.stunameSplit = function (arr1, arr2) {
        var _this = this;
        arr1.forEach(function (element) {
            _this.come.push(element);
        });
        arr2.forEach(function (element) {
            _this.all_class.push(element);
        });
        this.sum_studeninclass = this.stu_daily.length;
        this.set_sum();
    };
    CheckSumPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-check-sum',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\check-sum\check-sum.html"*/'<!--\n\n  Generated template for the CheckSumPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>ผลรวมการเช็คชื่อ</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <h1>จำนวนครั้งทั้งหมด {{sum_studeninclass}}</h1>\n\n    <ion-item *ngFor="let item1 of sum_data">\n\n        {{item1.id}} {{item1.name}}\n\n        <h1>รวม {{item1.sum}}</h1>\n\n    </ion-item>\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\check-sum\check-sum.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], CheckSumPage);
    return CheckSumPage;
}());

//# sourceMappingURL=check-sum.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateChecklistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__teacher_home_teacher_home__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CreateChecklistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreateChecklistPage = /** @class */ (function () {
    function CreateChecklistPage(navCtrl, navParams, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistservice = checklistservice;
        this.min = 0;
        this.sec = 0;
        this.stop_time = false;
    }
    CreateChecklistPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateChecklistPage');
        this.sjdata = this.navParams.data;
        this.subject_id = this.sjdata.subject_id;
        this.subject_name = this.sjdata.subject_name;
        this.group = this.sjdata.group;
        this.teacher_id = this.sjdata.teacher_id;
        this.teacher_name = this.sjdata.teacher_name;
    };
    CreateChecklistPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.checklistservice.getCodeCheck().subscribe(function (Response) {
            _this.code_server = Response.data;
            _this.makecodeforcheck();
        });
    };
    CreateChecklistPage.prototype.makecodeforcheck = function () {
        var _this = this;
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 10; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        this.code_server.forEach(function (element) {
            if (element.random_code == text) {
                _this.makecodeforcheck();
            }
            else {
                _this.randomCode = text;
            }
        });
        if (this.code_server.length == 0) {
            this.randomCode = text;
        }
    };
    CreateChecklistPage.prototype.clickTime = function () {
        this.stop_time = false;
        this.checklistservice.addCheckname(this.subject_id, this.subject_name, this.group, this.teacher_id, this.teacher_name, this.randomCode);
        this.checklistservice.addCodeCheck(this.randomCode);
        var intervalVar = setInterval(function () {
            this.sec++;
            if (this.stop_time == true) {
                clearInterval(intervalVar);
                this.checklistservice.clearCode(this.randomCode);
                this.checklistservice.deleteCodeCheck(this.randomCode);
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__teacher_home_teacher_home__["a" /* TeacherHomePage */], this.sjdata.teacherData);
            }
            if (this.sec == 60) {
                this.min++;
                this.sec = 0;
            }
        }.bind(this), 1000);
        // setInterval(function(){ this.timer++; }.bind(this),1000);
    };
    CreateChecklistPage.prototype.StopTime = function () {
        this.stop_time = true;
    };
    CreateChecklistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-create-checklist',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\create-checklist\create-checklist.html"*/'<!--\n\n  Generated template for the CreateChecklistPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <ion-title style="text-align: center">\n\n            รายวิชา\n\n        </ion-title>\n\n\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-list style="text-align: center">\n\n\n\n        <ion-label>\n\n            <h1>รหัสเช็คชื่อ: {{randomCode}}</h1>\n\n        </ion-label>\n\n\n\n\n\n\n\n    </ion-list>\n\n\n\n    <div text-center align-items-center>\n\n        <button ion-button color="secondary" (click)="clickTime()">เริ่มการเช็คชื่อ</button>\n\n    </div>\n\n    <div text-center align-items-center>\n\n        <button ion-button color="danger" (click)="StopTime()">หยุดการเช็คชื่อ</button>\n\n    </div>\n\n\n\n    <h3 style="text-align: center">{{min}} : {{sec}} วินาที</h3>\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\create-checklist\create-checklist.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], CreateChecklistPage);
    return CreateChecklistPage;
}());

//# sourceMappingURL=create-checklist.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateclassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__teacher_home_teacher_home__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the CreateclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreateclassPage = /** @class */ (function () {
    function CreateclassPage(navCtrl, navParams, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistservice = checklistservice;
    }
    CreateclassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateclassPage');
        this.teacherdata = this.navParams.data;
        this.teacherid = this.teacherdata._id;
        this.name = this.teacherdata.teacher_name;
    };
    CreateclassPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.checklistservice.getCodeJoine().subscribe(function (Response) {
            _this.code_server = Response.data;
        });
    };
    CreateclassPage.prototype.createsubject = function (id, namesubject, group) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.makeid();
                        return [4 /*yield*/, this.checklistservice.createClassSubject(id, namesubject, group, this.teacherid, this.name, this.code)];
                    case 1:
                        _a.sent();
                        this.checklistservice.addCodeJoine(this.code);
                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__teacher_home_teacher_home__["a" /* TeacherHomePage */], this.teacherdata);
                        return [2 /*return*/];
                }
            });
        });
    };
    CreateclassPage.prototype.makeid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var text, possible, i;
            return __generator(this, function (_a) {
                text = "";
                possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                for (i = 0; i < 5; i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                this.code_server.forEach(function (element) {
                    if (element.random_code == text) {
                        _this.makeid();
                    }
                    else {
                        _this.code = text;
                    }
                });
                if (this.code_server.length == 0) {
                    this.code = text;
                }
                return [2 /*return*/];
            });
        });
    };
    CreateclassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-createclass',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\createclass\createclass.html"*/'<!--\n\n  Generated template for the CreateclassPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>สร้างห้องเรียน</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-card>\n\n        <ion-card-content>\n\n            <ion-item>\n\n                <ion-input type="text" placeholder="รหัสวิชา-กลุ่ม" [value]="id" [(ngModel)]="id"></ion-input>\n\n                <br>\n\n                <ion-input type="text" placeholder="ชื่อวิชา" [value]="namesubject" [(ngModel)]="namesubject"></ion-input>\n\n                <br>\n\n                \n\n            </ion-item>\n\n\n\n        </ion-card-content>\n\n\n\n    </ion-card>\n\n    <br>\n\n    <div text-center align-items-center>\n\n        <button ion-button style="float:center" (click)="createsubject(id,namesubject,group)">สร้างห้องเรียน</button>\n\n    </div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\createclass\createclass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], CreateclassPage);
    return CreateclassPage;
}());

//# sourceMappingURL=createclass.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoTPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InfoTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfoTPage = /** @class */ (function () {
    function InfoTPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InfoTPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoTPage');
        this.teacherdata = this.navParams.data;
        this.name = this.teacherdata.teacher_name;
        this.faculty = this.teacherdata.faulty;
        this.email = this.teacherdata.email;
        console.log(this.teacherdata);
    };
    InfoTPage.prototype.logout = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    InfoTPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-info-t',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\info-t\info-t.html"*/'<!--\n\n  Generated template for the InfoTPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <div text-center>ข้อมูลอาจารย์</div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-label>ชื่อ-สกุล {{ name}}</ion-label>\n\n    <ion-label>คณะ {{faculty}} </ion-label>\n\n    <ion-label>Email {{email}} </ion-label>\n\n    <div text-center align-items-center>\n\n        <button ion-button (click)="logout()">ออกจากระบบ</button>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\info-t\info-t.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], InfoTPage);
    return InfoTPage;
}());

//# sourceMappingURL=info-t.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the StudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StudentPage = /** @class */ (function () {
    function StudentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    StudentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StudentPage');
    };
    StudentPage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    StudentPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    StudentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-student',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\student\student.html"*/'<!--\n\n  Generated template for the StudentPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title style="text-align: center">\n\n            <div text-center><h1>นักศึกษา</h1></div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <br>\n\n\n\n    <button ion-button color="primary" block (click)="login()">เข้าสู่ระบบ</button>\n\n    <br>\n\n\n\n\n\n    <button ion-button color="primary" block (click)="register()">ลงทะเบียน</button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\student\student.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], StudentPage);
    return StudentPage;
}());

//# sourceMappingURL=student.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__student_home_student_home__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_checklist_service_checklist_service__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, checklistsevice, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistsevice = checklistsevice;
        this.alertCtrl = alertCtrl;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.Home = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    LoginPage.prototype.loginStudent = function (id, password) {
        var _this = this;
        console.log(id);
        console.log(password);
        this.checklistsevice.loginstudent(id, password).subscribe(function (response) {
            if (response.data != null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__student_home_student_home__["a" /* StudentHomePage */], response.data);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: 'เข้าสู่ระบบไม่สำเร็จ',
                    message: 'กรุณาตรวจสอบอีเมล์หรือรหัสผ่าน',
                    buttons: [
                        {
                            text: 'ตกลง',
                            role: 'ยกเลิก',
                        }
                    ]
                });
                alert_1.present();
            }
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\login\login.html"*/'<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n\n\n        <ion-title>\n\n            <div text-center>เข้าสู่ระบบ</div>\n\n        </ion-title>\n\n\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-label floating>Email</ion-label>\n\n            <ion-input type="text" [value]="id" [(ngModel)]="id"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Password</ion-label>\n\n            <ion-input type="password" [value]="password" [(ngModel)]="password"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n    <div padding>\n\n        <button ion-button color="primary" block (click)="loginStudent(id,password);">ลงชื่อเข้าใช้</button>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JoinclassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



// import {StudentHomePage} from '../student-home/student-home';
/**
 * Generated class for the JoinclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var JoinclassPage = /** @class */ (function () {
    function JoinclassPage(navCtrl, navParams, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistservice = checklistservice;
    }
    JoinclassPage.prototype.ionViewDidLoad = function () {
        this.studentdata = this.navParams.data;
        this.stu_id = this.studentdata.student_id;
        this.stu_name = this.studentdata.student_name;
        console.log('ionViewDidLoad JoinclassPage');
    };
    JoinclassPage.prototype.gotoclassSubject = function (code_joine) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checklistservice.joineclass(this.stu_id, this.stu_name, code_joine)];
                    case 1:
                        _a.sent();
                        this.navCtrl.pop(this.studentdata);
                        return [2 /*return*/];
                }
            });
        });
    };
    JoinclassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-joinclass',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\joinclass\joinclass.html"*/'<!--\n\n  Generated template for the JoinclassPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>เข้าร่วมชั้นเรียน</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-card>\n\n\n\n\n\n\n\n        <ion-card-content>\n\n            <ion-item>\n\n                <ion-input type="text" placeholder="รหัสเข้าชั้นเรียน" [value]="code_joine" [(ngModel)]="code_joine"></ion-input>\n\n            </ion-item>\n\n        </ion-card-content>\n\n\n\n    </ion-card>\n\n    <br>\n\n    <div text-center align-items-center>\n\n        <button ion-button style="float:center" (click)="gotoclassSubject(code_joine)">เข้าร่วมชั้นเรียน</button>\n\n    </div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\joinclass\joinclass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], JoinclassPage);
    return JoinclassPage;
}());

//# sourceMappingURL=joinclass.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfoPage = /** @class */ (function () {
    function InfoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoPage');
        this.studentdata = this.navParams.data;
        this.name = this.studentdata.student_name;
        this.id = this.studentdata.student_id;
        this.faculty = this.studentdata.fauctly;
        this.major = this.studentdata.major;
        this.email = this.studentdata.email;
    };
    InfoPage.prototype.logout = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    InfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-info',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\info\info.html"*/'<!--\n\n  Generated template for the InfoPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <div text-center>\n\n            <ion-title>\n\n                <h1>ข้อมูลนิสิต</h1>\n\n            </ion-title>\n\n        </div>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n    <ion-label>ชื่อ-สกุล {{name}} </ion-label>\n\n    <ion-label>รหัสนิสิต {{id}} </ion-label>\n\n    <ion-label>คณะ {{faculty}} </ion-label>\n\n    <ion-label>สาขา {{major}} </ion-label>\n\n    <ion-label>Email {{email}} </ion-label>\n\n    <div text-center align-items-center>\n\n        <button ion-button (click)="logout()">ออกจากระบบ</button>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\info\info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], InfoPage);
    return InfoPage;
}());

//# sourceMappingURL=info.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InsidesubjectStudentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InsidesubjectStudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InsidesubjectStudentPage = /** @class */ (function () {
    function InsidesubjectStudentPage(navCtrl, navParams, checklistservice, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistservice = checklistservice;
        this.alertCtrl = alertCtrl;
        this.statuscode = false;
    }
    InsidesubjectStudentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InsidesubjectStudentPage');
        this.subjectdata = this.navParams.data;
        console.log(this.subjectdata);
        this.subjectname = this.subjectdata.subject_name;
        this.subjectid = this.subjectdata.subject_id;
        this.group = this.subjectdata.group;
        this.stu_id = this.subjectdata.student_id;
        this.stu_name = this.subjectdata.student_name;
    };
    InsidesubjectStudentPage.prototype.Stucheckname = function (codecheckname) {
        var _this = this;
        this.checklistservice.getCodeCheck().subscribe(function (Response) {
            if (Response.data.length != 0) {
                Response.data.forEach(function (element) {
                    if (element.code == codecheckname) {
                        _this.statuscode = true;
                    }
                    else {
                        _this.statuscode = false;
                    }
                });
            }
            if (_this.statuscode == true) {
                _this.checklistservice.checklistbyStu(_this.stu_id, _this.stu_name, codecheckname);
                var alert_1 = _this.alertCtrl.create({
                    title: 'สำเร็จ',
                    message: 'เช็คชื่อสำเร็จ',
                    buttons: [
                        {
                            text: 'โอเค',
                            role: 'ยกเลิก',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'ไม่สำเร็จ',
                    message: 'การเช็คชื่อไม่สำเร็จ',
                    buttons: [
                        {
                            text: 'ตกลง',
                            role: 'ยกเลิก',
                            handler: function () {
                            }
                        }
                    ]
                });
                alert_2.present();
            }
        });
    };
    InsidesubjectStudentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-insidesubject-student',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\insidesubject-student\insidesubject-student.html"*/'<!--\n\n  Generated template for the InsideSubjectPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title style="text-align: center">รายวิชา</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n    <ion-item>\n\n        <h3>ชื่อวิชา : {{subjectname}}</h3>\n\n    </ion-item>\n\n    <ion-item>\n\n        <h3>รหัสวิชา-กลุ่ม : {{subjectid}}</h3>\n\n    </ion-item>\n\n    \n\n\n\n    <br>\n\n    <ion-card>\n\n        <ion-card-content>\n\n            <ion-item>\n\n                <ion-label color="primary">กรอกโค้ด</ion-label>\n\n\n\n                <ion-input placeholder="กรุณาใส่โค้ด" [value]="codecheckname" [(ngModel)]="codecheckname"></ion-input>\n\n\n\n            </ion-item>\n\n        </ion-card-content>\n\n    </ion-card>\n\n\n\n    <ion-content padding>\n\n\n\n        <br>\n\n        <div text-center align-items-center>\n\n            <button ion-button (click)="Stucheckname(codecheckname)">เช็คชื่อ</button>\n\n\n\n        </div>\n\n\n\n    </ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\insidesubject-student\insidesubject-student.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], InsidesubjectStudentPage);
    return InsidesubjectStudentPage;
}());

//# sourceMappingURL=insidesubject-student.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__student_home_student_home__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_checklist_service_checklist_service__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, alertCtrl, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.checklistservice = checklistservice;
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.register = function (m, name, id, fauclty, major, email, password, confirmPassword) {
        var _this = this;
        if (m == null || name == null || id == null || fauclty == null || major == null || email == null || password == null || confirmPassword == null) {
            var alert_1 = this.alertCtrl.create({
                title: 'ลงทะเบียนไม่สำเร็จ',
                message: 'กรุณากรอกข้อมูลอีกครั้ง',
                buttons: [
                    {
                        text: 'ตกลง',
                        role: 'ยกเลิก',
                    }
                ]
            });
            alert_1.present();
        }
        else if (password != confirmPassword) {
            var alert_2 = this.alertCtrl.create({
                title: 'ลงทะเบียนไม่สำเร็จ',
                message: 'กรุณากรอกรหัสผ่านอีกครั้ง',
                buttons: [
                    {
                        text: 'ตกลง',
                        role: 'ยกเลิก',
                    }
                ]
            });
            alert_2.present();
        }
        else {
            var alert_3 = this.alertCtrl.create({
                title: 'สมัครใช้งานสำเร็จ',
                message: 'เราจะนำคุณเข้าสู่หน้า เข้าสู่ระบบ',
                buttons: [
                    {
                        text: 'โอเค',
                        role: 'ยกเลิก',
                        handler: function () {
                            _this.addUser(m, name, id, fauclty, major, email, password);
                        }
                    }
                ]
            });
            alert_3.present();
        }
    };
    RegisterPage.prototype.addUser = function (m, name, id, fauclty, major, email, password) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checklistservice.createUserStudent(m, name, id, fauclty, major, email, password)];
                    case 1:
                        _a.sent();
                        this.checklistservice.loginstudent(email, password).subscribe(function (response) {
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__student_home_student_home__["a" /* StudentHomePage */], response.data);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\register\register.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <div text-center>ลงทะเบียน</div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-label>ชื่อนำ</ion-label>\n\n            <ion-select [(ngModel)]="m">\n\n                <ion-option value="นาย">นาย</ion-option>\n\n                <ion-option value="นางสาว">นางสาว</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n    </ion-list>\n\n\n\n    <ion-item>\n\n        <ion-label fixed>ชื่อ-สกุล</ion-label>\n\n        <ion-input type="text" [value]="name" [(ngModel)]="name"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label fixed>รหัสนิสิต</ion-label>\n\n        <ion-input type="text" [value]="id" [(ngModel)]="id"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label fixed>คณะ</ion-label>\n\n        <ion-input type="text" [value]="fauclty" [(ngModel)]="fauclty"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label fixed>สาขา</ion-label>\n\n        <ion-input type="text" [value]="major" [(ngModel)]="major"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label fixed>Email</ion-label>\n\n        <ion-input type="Email" placeholder="59160792@go.buu.ac.th" [value]="email" [(ngModel)]="email"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <ion-label fixed>รหัสผ่าน</ion-label>\n\n        <ion-input type="password" [value]="password" [(ngModel)]="password"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <ion-label fixed>ยืนยันรหัสผ่าน </ion-label>\n\n        <ion-input type="password" [value]="confirmPassword" [(ngModel)]="confirmPassword"></ion-input>\n\n    </ion-item>\n\n\n\n    <div text-center align-items-center>\n\n        <button ion-button style="float:center" (click)="register(m,name,id,fauclty,major,email,password,confirmPassword)">ตกลง</button>\n\n    </div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeacherPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_t_login_t__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_t_register_t__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TeacherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TeacherPage = /** @class */ (function () {
    function TeacherPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TeacherPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TeacherPage');
    };
    TeacherPage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_t_login_t__["a" /* LoginTPage */]);
    };
    TeacherPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_t_register_t__["a" /* RegisterTPage */]);
    };
    TeacherPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-teacher',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\teacher\teacher.html"*/'<!--\n\n  Generated template for the TeacherPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <div text-center>อาจารย์</div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <br>\n\n\n\n    <button ion-button color="primary" block (click)="login()">เข้าสู่ระบบ</button>\n\n    <br>\n\n\n\n    <button ion-button color="primary" block (click)="register()">ลงทะเบียน</button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\teacher\teacher.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], TeacherPage);
    return TeacherPage;
}());

//# sourceMappingURL=teacher.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginTPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__teacher_home_teacher_home__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_checklist_service_checklist_service__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LoginTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginTPage = /** @class */ (function () {
    function LoginTPage(navCtrl, navParams, checklistsevice, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistsevice = checklistsevice;
        this.alertCtrl = alertCtrl;
    }
    LoginTPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginTPage');
    };
    LoginTPage.prototype.Home = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    LoginTPage.prototype.login = function (id, password) {
        var _this = this;
        this.checklistsevice.loginteacher(id, password).subscribe(function (response) {
            if (response.data != null) {
                console.log(response.data);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__teacher_home_teacher_home__["a" /* TeacherHomePage */], response.data);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: 'เข้าสู่ระบบไม่สำเร็จ',
                    message: 'กรุณาตรวจสอบอีเมล์หรือรหัสผ่าน',
                    buttons: [
                        {
                            text: 'ตกลง',
                            role: 'ยกเลิก',
                        }
                    ]
                });
                alert_1.present();
            }
        });
    };
    LoginTPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login-t',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\login-t\login-t.html"*/'<!--\n\n  Generated template for the LoginTPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <div text-center>เข้าสู่ระบบ</div>\n\n        </ion-title>\n\n\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-label floating>Email</ion-label>\n\n            <ion-input type="text" [value]="id" [(ngModel)]="id"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Password</ion-label>\n\n            <ion-input type="password" [value]="password" [(ngModel)]="password"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n    <div padding>\n\n        <button ion-button color="primary" block (click)="login(id,password);">ลงชื่อเข้าใช้</button>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\login-t\login-t.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], LoginTPage);
    return LoginTPage;
}());

//# sourceMappingURL=login-t.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterTPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__teacher_home_teacher_home__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_checklist_service_checklist_service__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the RegisterTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterTPage = /** @class */ (function () {
    function RegisterTPage(navCtrl, navParams, alertCtrl, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.checklistservice = checklistservice;
    }
    RegisterTPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterTPage');
    };
    RegisterTPage.prototype.resigterTeacher = function (m, name, faculty, email, password, confirmPassword) {
        var _this = this;
        if (m == null || name == null || faculty == null || email == null || password == null || confirmPassword == null) {
            var alert_1 = this.alertCtrl.create({
                title: 'ลงทะเบียนไม่สำเร็จ',
                message: 'กรุณากรอกข้อมูลอีกครั้ง',
                buttons: [
                    {
                        text: 'ตกลง',
                        role: 'ยกเลิก',
                    }
                ]
            });
            alert_1.present();
        }
        else if (password != confirmPassword) {
            var alert_2 = this.alertCtrl.create({
                title: 'ลงทะเบียนไม่สำเร็จ',
                message: 'กรุณากรอกรหัสผ่านอีกครั้ง',
                buttons: [
                    {
                        text: 'ตกลง',
                        role: 'ยกเลิก',
                    }
                ]
            });
            alert_2.present();
        }
        else {
            var alert_3 = this.alertCtrl.create({
                title: 'สมัครใช้งานสำเร็จ',
                message: 'เราจะนำคุณเข้าสู่หน้า เข้าสู่ระบบ',
                buttons: [
                    {
                        text: 'ตกลง',
                        role: 'ยกเลิก',
                        handler: function () {
                            _this.addUser(m, name, faculty, email, password);
                        }
                    }
                ]
            });
            alert_3.present();
        }
    };
    RegisterTPage.prototype.addUser = function (m, name, faculty, email, password) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checklistservice.createUserTeacher(m, name, faculty, email, password)];
                    case 1:
                        _a.sent();
                        this.checklistservice.loginteacher(email, password).subscribe(function (response) {
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__teacher_home_teacher_home__["a" /* TeacherHomePage */], response.data);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterTPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register-t',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\register-t\register-t.html"*/'<!--\n\n  Generated template for the RegisterTPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <div text-center>ลงทะเบียน</div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n\n\n    <ion-item>\n\n        <ion-label>ชื่อนำ</ion-label>\n\n        <ion-select [(ngModel)]="m">\n\n            <ion-option value="นาย">นาย</ion-option>\n\n            <ion-option value="นาง">นาง</ion-option>\n\n            <ion-option value="นางสาว">นางสาว</ion-option>\n\n        </ion-select>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <ion-label fixed>ชื่อ-สกุล</ion-label>\n\n        <ion-input type="text" [value]="name" [(ngModel)]="name"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <ion-label fixed>คณะ</ion-label>\n\n        <ion-input type="text" [value]="faculty" [(ngModel)]="faculty"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label fixed>Email</ion-label>\n\n        <ion-input type="Email" placeholder="xxx@xxxx.xxx" [value]="email" [(ngModel)]="email"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <ion-label fixed>รหัสผ่าน</ion-label>\n\n        <ion-input type="password" [value]="password" [(ngModel)]="password"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <ion-label fixed>ยืนยันรหัสผ่าน </ion-label>\n\n        <ion-input type="password" [value]="confirmPassword" [(ngModel)]="confirmPassword"></ion-input>\n\n    </ion-item>\n\n    <div text-center align-items-center>\n\n        <button ion-button style="float:center" (click)="resigterTeacher(m,name,faculty,email,password,confirmPassword)">ตกลง</button>\n\n    </div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\register-t\register-t.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], RegisterTPage);
    return RegisterTPage;
}());

//# sourceMappingURL=register-t.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InsideSubjectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_checklist_create_checklist__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__daily_check_list_daily_check_list__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__check_sum_check_sum__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the InsideSubjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InsideSubjectPage = /** @class */ (function () {
    function InsideSubjectPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InsideSubjectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InsideSubjectPage');
        this.subjectdata = this.navParams.data;
    };
    InsideSubjectPage.prototype.ionViewWillEnter = function () {
        this.subjectname = this.subjectdata.subject_name;
        this.subjectid = this.subjectdata.subject_id;
        this.group = this.subjectdata.group;
        this.code = this.subjectdata.code;
    };
    InsideSubjectPage.prototype.gocreatecheck = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__create_checklist_create_checklist__["a" /* CreateChecklistPage */], this.subjectdata);
    };
    InsideSubjectPage.prototype.gotodailylist = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__daily_check_list_daily_check_list__["a" /* DailyCheckListPage */], this.subjectdata);
    };
    InsideSubjectPage.prototype.gotocheck = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__check_sum_check_sum__["a" /* CheckSumPage */], this.subjectdata);
    };
    InsideSubjectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-inside-subject',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\inside-subject\inside-subject.html"*/'<!--\n\n  Generated template for the InsideSubjectPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title style="text-align: center">รายวิชา</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n    <ion-item>\n\n        <h3>ชื่อวิชา : {{subjectname}}</h3>\n\n    </ion-item>\n\n    <ion-item>\n\n        <h3>รหัสวิชา-กลุ่ม : {{subjectid}}</h3>\n\n    </ion-item>\n\n    \n\n    <br>\n\n    <div text-center align-items-center>\n\n        <button ion-button (click)="gocreatecheck()">สร้างการเช็คชื่อ</button>\n\n\n\n    </div>\n\n\n\n    <br>\n\n    <div text-center align-items-center>\n\n\n\n        <!-- <button ion-button style="float:center" (click)="gotocheck()">ผลรวมการเช็คชื่อ</button> -->\n\n        <button ion-button style="float:center" (click)="gotodailylist()">สรุปผลการเช็คชื่อรายวัน</button>\n\n    </div>\n\n    <br>\n\n    <ion-content padding>\n\n        <ion-card>\n\n\n\n\n\n\n\n            <ion-card-content>\n\n                <ion-item>\n\n                    <br>\n\n                    <h2>รหัสเข้าชั้นเรียน : {{code}}</h2>\n\n                </ion-item>\n\n            </ion-card-content>\n\n\n\n        </ion-card>\n\n        <br>\n\n\n\n    </ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\inside-subject\inside-subject.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], InsideSubjectPage);
    return InsideSubjectPage;
}());

//# sourceMappingURL=inside-subject.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyCheckListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dailyallshow_dailyallshow__ = __webpack_require__(118);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DailyCheckListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DailyCheckListPage = /** @class */ (function () {
    function DailyCheckListPage(navCtrl, navParams, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistservice = checklistservice;
        this.daily = [];
    }
    DailyCheckListPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.sjdata = this.navParams.data;
        console.log('ionViewDidLoad DailyCheckListPage');
        this.checklistservice.getCheckname().subscribe(function (Response) {
            _this.subjectid = _this.sjdata.subject_id;
            _this.subjectname = _this.sjdata.subject_name;
            _this.group = _this.sjdata.group;
            _this.code = _this.sjdata.code;
            _this.student = _this.sjdata.student;
            Response.data.forEach(function (element) {
                if (element.subject_id == _this.subjectid) {
                    var data = {
                        datetime: element.date_time,
                        sumstu: _this.student.length,
                        num_stu: element.student.length,
                        loststu: _this.student.length - element.student.length,
                        stu_daily: element.student,
                        stu_subject: _this.student
                    };
                    _this.daily.push(data);
                }
            });
        });
    };
    DailyCheckListPage.prototype.gotoshowall = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__dailyallshow_dailyallshow__["a" /* DailyallshowPage */], item);
    };
    DailyCheckListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-daily-check-list',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\daily-check-list\daily-check-list.html"*/'<!--\n\n  Generated template for the DailyCheckListPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>สรุปผลการเช็คชื่อรายวัน</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-item *ngFor="let item of daily">\n\n        <button ion-item (click)="gotoshowall(item)">\n\n   วันที่ {{item.datetime}}   <br>\n\n   จำนวนนิสิตที่มา {{item.num_stu}}  ขาด {{item.loststu}}\n\n\n\n   </button>\n\n    </ion-item>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\daily-check-list\daily-check-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], DailyCheckListPage);
    return DailyCheckListPage;
}());

//# sourceMappingURL=daily-check-list.js.map

/***/ }),

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyallshowPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the DailyallshowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DailyallshowPage = /** @class */ (function () {
    // no_come=[]
    function DailyallshowPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.come = [];
        this.all_class = [];
    }
    DailyallshowPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DailyallshowPage');
        this.dailydata = this.navParams.data;
        this.sumstu = this.dailydata.sumstu;
        this.loststu = this.dailydata.loststu;
        this.student_daily = this.dailydata.stu_daily;
        this.student = this.dailydata.stu_subject;
        this.stunameSplit(this.student_daily, this.student);
        console.log(this.lost);
    };
    DailyallshowPage.prototype.stunameSplit = function (arr1, arr2) {
        var _this = this;
        arr1.forEach(function (element) {
            _this.come.push(element.student_id + " " + element.student_name);
        });
        arr2.forEach(function (element) {
            _this.all_class.push(element.student_id + " " + element.student_name);
        });
        this.lost = this.arr_diff(this.come, this.all_class);
        console.log(this.lost);
    };
    DailyallshowPage.prototype.arr_diff = function (a1, a2) {
        var a = [], diff = [];
        for (var i = 0; i < a1.length; i++) {
            a[a1[i]] = true;
        }
        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {
                delete a[a2[i]];
            }
            else {
                a[a2[i]] = true;
            }
        }
        for (var k in a) {
            diff.push(k);
        }
        return diff;
    };
    DailyallshowPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dailyallshow',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\dailyallshow\dailyallshow.html"*/'<!--\n\n  Generated template for the DailyallshowPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>รายชื่อนิสิต\n\n        </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <h1>นิสิตที่มาเรียน</h1>\n\n    <ion-item *ngFor="let item1 of come">\n\n        {{item1}}\n\n    </ion-item>\n\n\n\n    <h1>นิสิตที่ขาด</h1>\n\n    <ion-item *ngFor="let item2 of lost">\n\n        {{item2}}\n\n    </ion-item>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\dailyallshow\dailyallshow.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], DailyallshowPage);
    return DailyallshowPage;
}());

//# sourceMappingURL=dailyallshow.js.map

/***/ }),

/***/ 128:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 128;

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChecklistServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(171);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { HttpClient } from '@angular/common/http';



/*
  Generated class for the ChecklistServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ChecklistServiceProvider = /** @class */ (function () {
    function ChecklistServiceProvider(http) {
        this.http = http;
        console.log('Hello ChecklistServiceProvider Provider');
    }
    ChecklistServiceProvider.prototype.loginstudent = function (id, password) {
        return this.http.get("http://192.168.43.181:8000/student/" + id + "/" + password).map(function (res) { return res.json(); });
    };
    ChecklistServiceProvider.prototype.loginteacher = function (id, password) {
        return this.http.get("http://192.168.43.181:8000/teacher/" + id + "/" + password).map(function (res) { return res.json(); });
    };
    ChecklistServiceProvider.prototype.createUserStudent = function (m, name, id, fauclty, major, email, password) {
        var data = {
            student_id: id,
            student_name: m + name,
            fauctly: fauclty,
            major: major,
            email: email,
            password: password
        };
        return this.http.post("http://192.168.43.181:8000/student/add", data).toPromise();
    };
    ChecklistServiceProvider.prototype.createUserTeacher = function (m, name, faculty, email, password) {
        var data = {
            teacher_name: m + name,
            faulty: faculty,
            email: email,
            password: password
        };
        return this.http.post("http://192.168.43.181:8000/teacher/add", data).toPromise();
    };
    ChecklistServiceProvider.prototype.createClassSubject = function (id, namesubject, group, teacher_id, teacher_name, code) {
        var data = {
            subject_id: id,
            subject_name: namesubject,
            group: group,
            teacher_id: teacher_id,
            teacher_name: teacher_name,
            student: [],
            code: code
        };
        return this.http.post("http://192.168.43.181:8000/subject/add", data).toPromise(); //  return this.http.post("http://localhost:8000/subject/add",data).toPromise();
    };
    ChecklistServiceProvider.prototype.getSubject = function () {
        return this.http.get("http://192.168.43.181:8000/subject").map(function (res) { return res.json(); });
    };
    ChecklistServiceProvider.prototype.getCodeJoine = function () {
        return this.http.get("http://192.168.43.181:8000/codejoine").map(function (res) { return res.json(); });
    };
    ChecklistServiceProvider.prototype.getCodeCheck = function () {
        return this.http.get("http://192.168.43.181:8000/codecheck").map(function (res) { return res.json(); });
    };
    ChecklistServiceProvider.prototype.addCodeJoine = function (code) {
        var data = {
            random_code: code,
        };
        return this.http.post("http://192.168.43.181:8000/codejoine/add", data).toPromise();
    };
    ChecklistServiceProvider.prototype.addCodeCheck = function (code) {
        var data = {
            code: code,
        };
        return this.http.post("http://192.168.43.181:8000/codecheck/add", data).toPromise();
    };
    ChecklistServiceProvider.prototype.deleteCodeCheck = function (code) {
        return this.http.delete("http://192.168.43.181:8000/codecheck/delete/" + code).toPromise();
    };
    ChecklistServiceProvider.prototype.addCheckname = function (subject_id, subject_name, group, teacher_id, teacher_name, codeCheck) {
        var dt = new Date().toLocaleString('en-GB', { timeZone: 'Asia/Bangkok' });
        var data = {
            student: [],
            subject_id: subject_id,
            subject_name: subject_name,
            group: group,
            teacher_id: teacher_id,
            teacher_name: teacher_name,
            date_time: dt,
            code: codeCheck,
        };
        return this.http.post("http://192.168.43.181:8000/checkname/add", data).toPromise();
    };
    ChecklistServiceProvider.prototype.clearCode = function (code) {
        console.log(code);
        return this.http.put("http://192.168.43.181:8000/clearCode/" + code, '').toPromise();
    };
    ChecklistServiceProvider.prototype.joineclass = function (student_id, student_name, code) {
        var data = {
            student_id: student_id,
            student_name: student_name
        };
        return this.http.put("http://192.168.43.181:8000/joinclasswithsubject/" + code, data).toPromise();
    };
    ChecklistServiceProvider.prototype.checklistbyStu = function (student_id, student_name, code) {
        var data = {
            student_id: student_id,
            student_name: student_name
        };
        return this.http.put("http://192.168.43.181:8000/checknameofStu/" + code, data).toPromise();
    };
    ChecklistServiceProvider.prototype.getCheckname = function () {
        return this.http.get("http://192.168.43.181:8000/checkname").map(function (res) { return res.json(); });
    };
    ChecklistServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]])
    ], ChecklistServiceProvider);
    return ChecklistServiceProvider;
}());

//# sourceMappingURL=checklist-service.js.map

/***/ }),

/***/ 170:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/check-sum/check-sum.module": [
		290,
		18
	],
	"../pages/create-checklist/create-checklist.module": [
		291,
		17
	],
	"../pages/create-group/create-group.module": [
		292,
		16
	],
	"../pages/createclass/createclass.module": [
		293,
		15
	],
	"../pages/daily-check-list/daily-check-list.module": [
		294,
		14
	],
	"../pages/dailyallshow/dailyallshow.module": [
		295,
		13
	],
	"../pages/info-t/info-t.module": [
		296,
		12
	],
	"../pages/info/info.module": [
		297,
		11
	],
	"../pages/inside-subject/inside-subject.module": [
		298,
		10
	],
	"../pages/insidesubject-student/insidesubject-student.module": [
		299,
		9
	],
	"../pages/joinclass/joinclass.module": [
		300,
		8
	],
	"../pages/login-t/login-t.module": [
		301,
		7
	],
	"../pages/login/login.module": [
		302,
		6
	],
	"../pages/register-t/register-t.module": [
		303,
		5
	],
	"../pages/register/register.module": [
		304,
		4
	],
	"../pages/student-home/student-home.module": [
		305,
		3
	],
	"../pages/student/student.module": [
		306,
		2
	],
	"../pages/teacher-home/teacher-home.module": [
		307,
		1
	],
	"../pages/teacher/teacher.module": [
		308,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 170;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateGroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CreateGroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreateGroupPage = /** @class */ (function () {
    function CreateGroupPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CreateGroupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateGroupPage');
    };
    CreateGroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-create-group',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\create-group\create-group.html"*/'<!--\n\n  Generated template for the CreateGroupPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button style="float:left" (click)="goinfo()">ข้อมูล</button>\n\n        <ion-title>\n\n            Student\n\n        </ion-title>\n\n        <button ion-button style="float:right" (click)="gocreateclass()"><ion-icon name="add-circle"></ion-icon></button>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <h3>รายวิชา</h3>\n\n    <div text-center align-items-center>\n\n        <button ion-button style="float:center">ok</button>\n\n    </div>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\create-group\create-group.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], CreateGroupPage);
    return CreateGroupPage;
}());

//# sourceMappingURL=create-group.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(239);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_teacher_home_teacher_home__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_createclass_createclass__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_joinclass_joinclass__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_register_register__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_register_t_register_t__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_student_student__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_student_home_student_home__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_teacher_teacher__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_checklist_service_checklist_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_http__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_info_info__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_info_t_info_t__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_login_t_login_t__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_create_group_create_group__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_inside_subject_inside_subject__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_create_checklist_create_checklist__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_insidesubject_student_insidesubject_student__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_check_sum_check_sum__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_daily_check_list_daily_check_list__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_dailyallshow_dailyallshow__ = __webpack_require__(118);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_teacher_home_teacher_home__["a" /* TeacherHomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_createclass_createclass__["a" /* CreateclassPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_joinclass_joinclass__["a" /* JoinclassPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_register_t_register_t__["a" /* RegisterTPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_student_student__["a" /* StudentPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_student_home_student_home__["a" /* StudentHomePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_teacher_teacher__["a" /* TeacherPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_info_info__["a" /* InfoPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_info_t_info_t__["a" /* InfoTPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_login_t_login_t__["a" /* LoginTPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_create_group_create_group__["a" /* CreateGroupPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_inside_subject_inside_subject__["a" /* InsideSubjectPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_create_checklist_create_checklist__["a" /* CreateChecklistPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_insidesubject_student_insidesubject_student__["a" /* InsidesubjectStudentPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_check_sum_check_sum__["a" /* CheckSumPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_daily_check_list_daily_check_list__["a" /* DailyCheckListPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_dailyallshow_dailyallshow__["a" /* DailyallshowPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/check-sum/check-sum.module#CheckSumPageModule', name: 'CheckSumPage', segment: 'check-sum', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-checklist/create-checklist.module#CreateChecklistPageModule', name: 'CreateChecklistPage', segment: 'create-checklist', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-group/create-group.module#CreateGroupPageModule', name: 'CreateGroupPage', segment: 'create-group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/createclass/createclass.module#CreateclassPageModule', name: 'CreateclassPage', segment: 'createclass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-check-list/daily-check-list.module#DailyCheckListPageModule', name: 'DailyCheckListPage', segment: 'daily-check-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dailyallshow/dailyallshow.module#DailyallshowPageModule', name: 'DailyallshowPage', segment: 'dailyallshow', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/info-t/info-t.module#InfoTPageModule', name: 'InfoTPage', segment: 'info-t', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/info/info.module#InfoPageModule', name: 'InfoPage', segment: 'info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inside-subject/inside-subject.module#InsideSubjectPageModule', name: 'InsideSubjectPage', segment: 'inside-subject', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/insidesubject-student/insidesubject-student.module#InsidesubjectStudentPageModule', name: 'InsidesubjectStudentPage', segment: 'insidesubject-student', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/joinclass/joinclass.module#JoinclassPageModule', name: 'JoinclassPage', segment: 'joinclass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login-t/login-t.module#LoginTPageModule', name: 'LoginTPage', segment: 'login-t', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register-t/register-t.module#RegisterTPageModule', name: 'RegisterTPage', segment: 'register-t', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/student-home/student-home.module#StudentHomePageModule', name: 'StudentHomePage', segment: 'student-home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/student/student.module#StudentPageModule', name: 'StudentPage', segment: 'student', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/teacher-home/teacher-home.module#TeacherHomePageModule', name: 'TeacherHomePage', segment: 'teacher-home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/teacher/teacher.module#TeacherPageModule', name: 'TeacherPage', segment: 'teacher', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_17__angular_http__["b" /* HttpModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_teacher_home_teacher_home__["a" /* TeacherHomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_createclass_createclass__["a" /* CreateclassPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_joinclass_joinclass__["a" /* JoinclassPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_register_t_register_t__["a" /* RegisterTPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_student_student__["a" /* StudentPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_student_home_student_home__["a" /* StudentHomePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_teacher_teacher__["a" /* TeacherPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_info_info__["a" /* InfoPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_info_t_info_t__["a" /* InfoTPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_login_t_login_t__["a" /* LoginTPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_create_group_create_group__["a" /* CreateGroupPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_inside_subject_inside_subject__["a" /* InsideSubjectPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_create_checklist_create_checklist__["a" /* CreateChecklistPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_insidesubject_student_insidesubject_student__["a" /* InsidesubjectStudentPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_check_sum_check_sum__["a" /* CheckSumPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_daily_check_list_daily_check_list__["a" /* DailyCheckListPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_dailyallshow_dailyallshow__["a" /* DailyallshowPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_16__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n<!-- <ion-navbar color="dark">\n\n    <ion-tab tabIcon="home" tabTitle="หน้าแรก" [root]="rootRoot"></ion-tab>\n\n    <ion-tab tabIcon="list" tabTitle="โปรไฟล์" [root]="rootRoot"></ion-tab>\n\n\n\n    </ion-tabs> -->'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__student_student__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__teacher_teacher__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage.prototype.Student = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__student_student__["a" /* StudentPage */]);
    };
    HomePage.prototype.teacher = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__teacher_teacher__["a" /* TeacherPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\home\home.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <div text-center>\n\n                <h1>ระบบเช็คชื่อ</h1>\n\n            </div>\n\n        </ion-title>\n\n\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item (click)="Student()">\n\n\n\n            <ion-card>\n\n                <img src="http://www.kobe11achillesheel.com/reviews/img/girl-21.png">\n\n            </ion-card>\n\n\n\n            <div text-center>\n\n                <h1>นักศึกษา</h1>\n\n            </div>\n\n\n\n        </ion-item>\n\n    </ion-list>\n\n\n\n    <ion-list>\n\n        <ion-item (click)="teacher()">\n\n\n\n            <ion-card>\n\n                <img src="https://images.vexels.com/media/users/3/141633/isolated/preview/a2bf2ebaf3f1c560e74eaca740ff1918-curly-hair-woman-teacher-by-vexels.png">\n\n            </ion-card>\n\n\n\n            <div text-center>\n\n                <h1>อาจารย์</h1>\n\n            </div>\n\n\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeacherHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__createclass_createclass__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__info_t_info_t__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_checklist_service_checklist_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__inside_subject_inside_subject__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the TeacherHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TeacherHomePage = /** @class */ (function () {
    function TeacherHomePage(navCtrl, navParams, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistservice = checklistservice;
        this.subjectdataTeacher = [];
    }
    TeacherHomePage.prototype.ionViewDidLoad = function () {
        this.teacherdata = this.navParams.data;
        console.log(this.teacherdata);
        console.log('ionViewDidLoad TeacherHomePage');
        // this.checklistservice.getSubject().subscribe((Response)=>{
        //   this.subjectdata=Response.data
        //   this.seperate()
        // })
    };
    TeacherHomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.checklistservice.getSubject().subscribe(function (Response) {
            _this.subjectdataTeacher = [];
            _this.subjectdata = Response.data;
            _this.seperate();
        });
    };
    TeacherHomePage.prototype.seperate = function () {
        var _this = this;
        this.subjectdata.forEach(function (element) {
            if (element.teacher_id == _this.teacherdata._id) {
                // var data={
                //   _id:element._id,
                //  subject_id:element.subject_id,
                //  subject_name:element.subject_name,
                //  group:element.group,
                //  teacher_id:element.teacher_id,
                //  teacher_name:element.teacher_name,
                //  student:element.student,
                //  code:element.code
                // }
                _this.subjectdataTeacher.push(element);
            }
        });
    };
    TeacherHomePage.prototype.gocreateclass = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__createclass_createclass__["a" /* CreateclassPage */], this.teacherdata);
    };
    TeacherHomePage.prototype.goinfo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__info_t_info_t__["a" /* InfoTPage */], this.teacherdata);
    };
    TeacherHomePage.prototype.goinside = function (item) {
        var data = {
            _id: item._id,
            subject_id: item.subject_id,
            subject_name: item.subject_name,
            group: item.group,
            teacher_id: item.teacher_id,
            teacher_name: item.teacher_name,
            student: item.student,
            code: item.code,
            teacherData: this.teacherdata
        };
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__inside_subject_inside_subject__["a" /* InsideSubjectPage */], data);
    };
    TeacherHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-teacher-home',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\teacher-home\teacher-home.html"*/'<!--\n\n  Generated template for the TeacherHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n\n\n    <ion-navbar>\n\n\n\n        <button ion-button style="float:left" (click)="goinfo()">ข้อมูล</button>\n\n\n\n        <button ion-fab style="float:right" (click)="gocreateclass()"><ion-icon name="add-circle"></ion-icon></button>\n\n\n\n        <ion-title style="text-align: center">\n\n            <div text-center>\n\n                หน้าหลัก\n\n            </div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n\n\n    <h3>รายวิชา</h3>\n\n    <ion-list>\n\n        <ion-item *ngFor="let item of subjectdataTeacher">\n\n            <button ion-item (click)="goinside(item)">\n\n             รหัสวิชา {{item.subject_id}}  {{item.subject_name}}  \n\n            </button>\n\n        </ion-item>\n\n    </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\teacher-home\teacher-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], TeacherHomePage);
    return TeacherHomePage;
}());

//# sourceMappingURL=teacher-home.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__joinclass_joinclass__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__info_info__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_checklist_service_checklist_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__insidesubject_student_insidesubject_student__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the StudentHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StudentHomePage = /** @class */ (function () {
    function StudentHomePage(navCtrl, navParams, checklistservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.checklistservice = checklistservice;
        this.subjectdataStudent = [];
    }
    StudentHomePage.prototype.ionViewDidLoad = function () {
        this.studentdata = this.navParams.data;
        this.student_id = this.studentdata.student_id;
        this.student_name = this.studentdata.student_name;
        console.log('ionViewDidLoad StudentHomePage');
    };
    StudentHomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.checklistservice.getSubject().subscribe(function (Response) {
            _this.subjectdataStudent = [];
            _this.subjectdata = Response.data;
            _this.seperate();
        });
    };
    StudentHomePage.prototype.gojoinclass = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__joinclass_joinclass__["a" /* JoinclassPage */], this.studentdata);
    };
    StudentHomePage.prototype.goinfo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__info_info__["a" /* InfoPage */], this.studentdata);
    };
    StudentHomePage.prototype.seperate = function () {
        var _this = this;
        this.subjectdata.forEach(function (element) {
            element.student.forEach(function (element1) {
                if (element1.student_id == _this.student_id)
                    _this.subjectdataStudent.push(element);
            });
        });
    };
    StudentHomePage.prototype.goinsideSt = function (item) {
        var data = {
            subject_id: item.subject_id,
            subject_name: item.subject_name,
            group: item.group,
            student_id: this.student_id,
            student_name: this.student_name
        };
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__insidesubject_student_insidesubject_student__["a" /* InsidesubjectStudentPage */], data);
    };
    StudentHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-student-home',template:/*ion-inline-start:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\student-home\student-home.html"*/'<!--\n\n  Generated template for the StudentHomePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button style="float:left" (click)="goinfo()">ข้อมูล</button>\n\n        <button ion-fab style="float:right" (click)="gojoinclass()"><ion-icon name="add-circle"></ion-icon></button>\n\n\n\n        <ion-title>\n\n            <div text-center>หน้าหลัก</div>\n\n\n\n        </ion-title>\n\n\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n\n\n    <h3>รายวิชา</h3>\n\n    <ion-list>\n\n        <ion-item *ngFor="let item of subjectdataStudent">\n\n            <button ion-item (click)="goinsideSt(item)">\n\n             รหัสวิชา {{item.subject_id}}  {{item.subject_name}}  กลุ่ม {{item.group}}\n\n            </button>\n\n        </ion-item>\n\n    </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\MonaZa55007\Desktop\pm\checklistapplication\src\pages\student-home\student-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_checklist_service_checklist_service__["a" /* ChecklistServiceProvider */]])
    ], StudentHomePage);
    return StudentHomePage;
}());

//# sourceMappingURL=student-home.js.map

/***/ })

},[216]);
//# sourceMappingURL=main.js.map