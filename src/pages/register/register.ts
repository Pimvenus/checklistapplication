import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {StudentHomePage} from '../student-home/student-home'
import {ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service'

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})


export class RegisterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController,private checklistservice:ChecklistServiceProvider ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  register(m,name,id,fauclty,major,email,password,confirmPassword){

    if (m==null || name== null || id== null || fauclty== null || major==null || email ==null || password==null || confirmPassword == null){
      let alert = this.alertCtrl.create({
        title: 'ลงทะเบียนไม่สำเร็จ',
        message: 'กรุณากรอกข้อมูลอีกครั้ง',
        buttons: [
          {
            text: 'ตกลง',
            role: 'ยกเลิก',
           
          }
        ]
      });
      alert.present();
    }else if(password != confirmPassword){
      let alert = this.alertCtrl.create({
        title: 'ลงทะเบียนไม่สำเร็จ',
        message: 'กรุณากรอกรหัสผ่านอีกครั้ง',
        buttons: [
          {
            text: 'ตกลง',
            role: 'ยกเลิก',
           
          }
        ]
      });
      alert.present();
      
    }else{
      let alert = this.alertCtrl.create({
        title: 'สมัครใช้งานสำเร็จ',
        message: 'เราจะนำคุณเข้าสู่หน้า เข้าสู่ระบบ',
        buttons: [
          {
            text: 'โอเค',
            role: 'ยกเลิก',
            handler: () => {
              this.addUser(m,name,id,fauclty,major,email,password);
             
            }
          }
        ]
      });
     alert.present();
    }
  


  }
    async addUser(m,name,id,fauclty,major,email,password){
      await this.checklistservice.createUserStudent(m,name,id,fauclty,major,email,password);
      this.checklistservice.loginstudent(email,password).subscribe((response) => {
        this.navCtrl.setRoot(StudentHomePage,response.data)   
      })
     
    }
  }