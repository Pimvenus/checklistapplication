import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InsideSubjectPage } from './inside-subject';

@NgModule({
  declarations: [
    InsideSubjectPage,
  ],
  imports: [
    IonicPageModule.forChild(InsideSubjectPage),
  ],
})
export class InsideSubjectPageModule {}
