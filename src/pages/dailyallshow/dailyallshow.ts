import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the DailyallshowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dailyallshow',
  templateUrl: 'dailyallshow.html',
})
export class DailyallshowPage {
dailydata:any
sumstu:any
loststu:any
student_daily:Array<any>
student:Array<any>
lost:Array<any>
come=[]
all_class=[]
// no_come=[]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DailyallshowPage');
    this.dailydata=this.navParams.data
    this.sumstu=this.dailydata.sumstu
    this.loststu=this.dailydata.loststu
    this.student_daily =this.dailydata.stu_daily
    this.student=this.dailydata.stu_subject
    this.stunameSplit(this.student_daily,this.student)

   
console.log(this.lost);

}

stunameSplit(arr1,arr2){

arr1.forEach(element => {
  this.come.push(element.student_id+" "+element.student_name)
});

arr2.forEach(element => {
  this.all_class.push(element.student_id+" "+element.student_name)
});

this.lost=this.arr_diff(this.come,this.all_class)
console.log(this.lost);

}

arr_diff (a1, a2) {

  var a = [], diff = [];

  for (let i = 0; i < a1.length; i++) {
      a[a1[i]] = true;
  }

  for (let i = 0; i < a2.length; i++) {
      if (a[a2[i]]) {
          delete a[a2[i]];
      } else {
          a[a2[i]] = true;
      }
  }

  for (var k in a) {
      diff.push(k);
  }

  return diff;
}

}