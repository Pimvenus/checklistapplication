import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service';
import { TeacherHomePage } from '../teacher-home/teacher-home';
/**
 * Generated class for the CreateChecklistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-checklist',
  templateUrl: 'create-checklist.html',
})
export class CreateChecklistPage {
sjdata:any
randomCode:any
code_server:Array<any>

public min = 0
public sec = 0
public stop_time = false

subject_id:any
subject_name:any
group:any
teacher_id:any
teacher_name:any


  constructor(public navCtrl: NavController, public navParams: NavParams,private checklistservice:ChecklistServiceProvider) {
  }

  ionViewDidLoad() {
   console.log('ionViewDidLoad CreateChecklistPage');
   this.sjdata=this.navParams.data
 this.subject_id=this.sjdata.subject_id
 this.subject_name=this.sjdata.subject_name
 this.group=this.sjdata.group
 this.teacher_id=this.sjdata.teacher_id
 this.teacher_name=this.sjdata.teacher_name
  
  }
  ionViewWillEnter(){
    this.checklistservice.getCodeCheck().subscribe((Response) => {
      this.code_server = Response.data
      this.makecodeforcheck()
    })
  }
  makecodeforcheck() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 10; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
      
      this.code_server.forEach(element =>{
        if(element.random_code==text){
          this.makecodeforcheck()
        }else{
          this.randomCode=text;
        }
      })
      if( this.code_server.length==0){
        this.randomCode=text;
      }
}

 clickTime(){
  this.stop_time = false
  this.checklistservice.addCheckname(this.subject_id,this.subject_name,this.group,this.teacher_id,this.teacher_name,this.randomCode)
  this.checklistservice.addCodeCheck(this.randomCode)
  var intervalVar = setInterval(function(){
    this.sec++;
    if(this.stop_time ==true){
      clearInterval(intervalVar);
      this.checklistservice.clearCode(this.randomCode);
      this.checklistservice.deleteCodeCheck(this.randomCode);
      this.navCtrl.setRoot(TeacherHomePage,this.sjdata.teacherData);
    }
    if( this.sec == 60){
      this.min++;
      this.sec=0;
    }
      
  }.bind(this),1000)

  // setInterval(function(){ this.timer++; }.bind(this),1000);
}

StopTime() {
  this.stop_time = true
}




}
