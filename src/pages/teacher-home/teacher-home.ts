import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CreateclassPage} from '../createclass/createclass';
import { InfoTPage } from '../info-t/info-t';
import { ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service';
import { InsideSubjectPage } from '../inside-subject/inside-subject';

/**
 * Generated class for the TeacherHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teacher-home',
  templateUrl: 'teacher-home.html',
})
export class TeacherHomePage {
teacherdata:any
subjectdata:Array<any>
subjectdataTeacher=[]
  constructor(public navCtrl: NavController, public navParams: NavParams,private checklistservice:ChecklistServiceProvider ) {
  }

  ionViewDidLoad() {
    this.teacherdata=this.navParams.data
    console.log(this.teacherdata);
    console.log('ionViewDidLoad TeacherHomePage');
    // this.checklistservice.getSubject().subscribe((Response)=>{
    //   this.subjectdata=Response.data
    //   this.seperate()
    // })
     
    
  }

  ionViewWillEnter(){
   this.checklistservice.getSubject().subscribe((Response)=>{
     this.subjectdataTeacher=[]
     this.subjectdata=Response.data
     this.seperate()
   })
    
  }

  seperate(){
    this.subjectdata.forEach(element =>{
      if(element.teacher_id==this.teacherdata._id){
        // var data={
        //   _id:element._id,
        //  subject_id:element.subject_id,
        //  subject_name:element.subject_name,
        //  group:element.group,
        //  teacher_id:element.teacher_id,
        //  teacher_name:element.teacher_name,
        //  student:element.student,
        //  code:element.code
        // }
        this.subjectdataTeacher.push(element)
      }
    })
  }
  gocreateclass(){
    this.navCtrl.push(CreateclassPage,this.teacherdata);
  }
  goinfo(){
    this.navCtrl.push(InfoTPage,this.teacherdata);
  }
  goinside(item){
    
         var data={
          _id:item._id,
         subject_id:item.subject_id,
         subject_name:item.subject_name,
         group:item.group,
         teacher_id:item.teacher_id,
         teacher_name:item.teacher_name,
         student:item.student,
         code:item.code,
         teacherData:this.teacherdata
        
    }
    this.navCtrl.push(InsideSubjectPage,data)
 
  
  }
  
}
