import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage } from '../home/home';

/**
 * Generated class for the InfoTPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info-t',
  templateUrl: 'info-t.html',
})
export class InfoTPage {

teacherdata:any

name:any
faculty:any
email:any

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoTPage');
    this.teacherdata=this.navParams.data
    this.name = this.teacherdata.teacher_name
    this.faculty=this.teacherdata.faulty
    this.email=this.teacherdata.email
    
    console.log(this.teacherdata);
    
  }
logout(){
  this.navCtrl.setRoot(HomePage)
}
}
