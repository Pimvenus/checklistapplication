import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service';
import {TeacherHomePage } from'../teacher-home/teacher-home';


/**
 * Generated class for the CreateclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-createclass',
  templateUrl: 'createclass.html',
})
export class CreateclassPage {
  teacherdata:any

teacherid:any
name:any
code:any
code_server:Array<any>
  constructor(public navCtrl: NavController, public navParams: NavParams,private checklistservice:ChecklistServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateclassPage');
    this.teacherdata=this.navParams.data
    this.teacherid=this.teacherdata._id
    this.name = this.teacherdata.teacher_name
  
  }
  ionViewWillEnter(){
    this.checklistservice.getCodeJoine().subscribe((Response) => {
      this.code_server = Response.data
    })
  }
  async createsubject(id,namesubject,group){
    this.makeid();
    await this.checklistservice.createClassSubject(id,namesubject,group,this.teacherid,this.name,this.code)
    this.checklistservice.addCodeJoine(this.code)
    this.navCtrl.setRoot(TeacherHomePage,this.teacherdata)
  }
  async makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 5; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
      
      this.code_server.forEach(element =>{
        if(element.random_code==text){
          this.makeid()
        }else{
          this.code=text;
        }
      })
      if( this.code_server.length==0){
        this.code=text;
      }
    
  }
}