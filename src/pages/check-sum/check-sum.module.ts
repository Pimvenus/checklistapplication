import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckSumPage } from './check-sum';

@NgModule({
  declarations: [
    CheckSumPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckSumPage),
  ],
})
export class CheckSumPageModule {}
