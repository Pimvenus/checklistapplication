import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyCheckListPage } from './daily-check-list';

@NgModule({
  declarations: [
    DailyCheckListPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyCheckListPage),
  ],
})
export class DailyCheckListPageModule {}
