import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyallshowPage } from './dailyallshow';

@NgModule({
  declarations: [
    DailyallshowPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyallshowPage),
  ],
})
export class DailyallshowPageModule {}
