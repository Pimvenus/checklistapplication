import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {JoinclassPage} from '../joinclass/joinclass';
import { InfoPage } from '../info/info';
import { ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service';
import {InsidesubjectStudentPage}from '../insidesubject-student/insidesubject-student';
/**
 * Generated class for the StudentHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-student-home',
  templateUrl: 'student-home.html',
})
export class StudentHomePage {
  studentdata:any
  subjectdataStudent=[]
  subjectdata:Array<any>
  student_id:any
  student_name:any

  constructor(public navCtrl: NavController, public navParams: NavParams,private checklistservice:ChecklistServiceProvider) {
  }

  ionViewDidLoad() {
    this.studentdata=this.navParams.data
    this.student_id=this.studentdata.student_id
    this.student_name=this.studentdata.student_name
    console.log('ionViewDidLoad StudentHomePage');
  }

  ionViewWillEnter(){
    this.checklistservice.getSubject().subscribe((Response)=>{
      this.subjectdataStudent=[]
      this.subjectdata=Response.data
      this.seperate()
    })
     
   }
  gojoinclass(){
    
    this.navCtrl.push(JoinclassPage,this.studentdata);
  }
  goinfo(){
    this.navCtrl.push(InfoPage,this.studentdata);
  }
  seperate(){
    this.subjectdata.forEach(element =>{
    element.student.forEach(element1 => {
      if(element1.student_id==this.student_id)
      this.subjectdataStudent.push(element)
    });
    })
  }
  goinsideSt(item){
    var data={
      subject_id:item.subject_id,
      subject_name:item.subject_name,
      group:item.group,
      student_id:this.student_id,
      student_name:this.student_name
    }
this.navCtrl.push(InsidesubjectStudentPage,data)
  }
}
