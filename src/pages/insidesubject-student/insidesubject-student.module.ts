import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InsidesubjectStudentPage } from './insidesubject-student';

@NgModule({
  declarations: [
    InsidesubjectStudentPage,
  ],
  imports: [
    IonicPageModule.forChild(InsidesubjectStudentPage),
  ],
})
export class InsidesubjectStudentPageModule {}
