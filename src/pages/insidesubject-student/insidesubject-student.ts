import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ChecklistServiceProvider} from '../../providers/checklist-service/checklist-service';

/**
 * Generated class for the InsidesubjectStudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-insidesubject-student',
  templateUrl: 'insidesubject-student.html',
})
export class InsidesubjectStudentPage {
  subjectdata:any
  subjectname:any
  subjectid:any
  group:any
stu_id:any
stu_name:any
statuscode=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,private checklistservice:ChecklistServiceProvider,private alertCtrl: AlertController ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InsidesubjectStudentPage');
    this.subjectdata=this.navParams.data
    console.log(this.subjectdata);
    
    this.subjectname=this.subjectdata.subject_name
    this.subjectid=this.subjectdata.subject_id
    this.group=this.subjectdata.group
  this.stu_id=this.subjectdata.student_id
  this.stu_name=this.subjectdata.student_name
  }
 Stucheckname(codecheckname){
  
  
  


 this.checklistservice.getCodeCheck().subscribe((Response)=>{
  

if(Response.data.length != 0){
  Response.data.forEach(element => {

    if( element.code == codecheckname ){
      this.statuscode=true
    }else{
      this.statuscode=false
    }
    
  });
}

if( this.statuscode==true){
  this.checklistservice.checklistbyStu(this.stu_id,this.stu_name,codecheckname)
  let alert = this.alertCtrl.create({
    title: 'สำเร็จ',
    message: 'เช็คชื่อสำเร็จ',
    buttons: [
      {
        text: 'โอเค',
        role: 'ยกเลิก',
        handler: () => {
          this.navCtrl.pop();
         
        }
      }
    ]
  });
 alert.present();
 
}else{
  let alert = this.alertCtrl.create({
    title: 'ไม่สำเร็จ',
    message: 'การเช็คชื่อไม่สำเร็จ',
    buttons: [
      {
        text: 'ตกลง',
        role: 'ยกเลิก',
        handler: () => {
          
         
        }
      }
    ]
  });
 alert.present();
}
  

})
  
       

}
 
}
